/* global Stripe */
(function (Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.mybehavior = {
    attach: function (context, settings) {
      // Create a Stripe client
      var stripe = Stripe(drupalSettings.stripe_form.elements.stripe_pubkey);

      var card = stripe.elements().create('card');
      card.mount('#card-element');

      var form = document.getElementById('stripe-form');
      function handler(event) {
        event.preventDefault();

        var options = {name: document.getElementById('stripe-name').value};
        stripe.createToken(card, options).then(function (result) {
          if (result.error) {
            // Inform the user if there was an error
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
          }
          else {
            // Send the token to your server
            form.removeEventListener('submit', handler);
            form.querySelector('#stripe-token').value = result.token.id;
            form.submit();
          }
        });
      }

      form.addEventListener('submit', handler);
    }
  };

})(Drupal, drupalSettings);
