Problem:

Create a donation form that integrates with Stripe.

Guidelines:

Your site with the donation form should work with no more than two installation steps:

- installing drupal
- enabling one custom module containing your configuration and custom code.

Track the following information:

- Who is making the donation
- Donation amount
- Collect the credit card payment
