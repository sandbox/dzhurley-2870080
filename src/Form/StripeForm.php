<?php

namespace Drupal\stripe_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

require_once(__DIR__ . '/../../vendor/autoload.php');

class StripeForm extends FormBase {

  public function getFormId() {
    return 'stripe_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name'),
      '#attributes' => array('id' => 'stripe-name'),
      '#required' => TRUE,
    ];

    $form['amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount (USD)'),
      '#required' => TRUE,
    ];

    $form['card-label'] = [
      '#type' => 'label',
      '#title' => $this->t('Credit Card'),
      '#required' => TRUE,
    ];

    $form['card-input'] = [
      '#type' => 'container',
      '#attributes' => array('id' => 'card-element')
    ];

    $form['token'] = [
      '#type' => 'hidden',
      '#attributes' => array('id' => 'stripe-token')
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('stripe_form.settings');
    \Stripe\Stripe::setApiKey($config->get('stripe_form_key_secret'));

    $charge = \Stripe\Charge::create(array(
      // the Stripe API takes `amount` in cents, convert to whole dollars.
      'amount' => $form_state->getValue('amount') * 100,
      'currency' => 'usd',
      'description' => 'ThinkShout Donation',
      'source' => $form_state->getValue('token'),
    ));

    if ($charge->paid === true) {
      drupal_set_message($this->t('Thank you. Payment has been processed.'));
    }
    else {
      drupal_set_message($this->t('Payment failed.'), 'error');
    }
  }
}
